(function ($) {
	$(document).ready(function(){
		window.setInterval(updateClock, 1000);
		var current_hours = Drupal.settings['js_clock']['current_hours'];
		var current_minutes = Drupal.settings['js_clock']['current_minutes'];
		var current_seconds = Drupal.settings['js_clock']['current_seconds'];
		var real_hours = current_hours;

		function updateClock () {
			current_minutes = ( current_minutes < 10 ? "0" : "" ) + current_minutes;
			current_seconds = ( current_seconds < 10 ? "0" : "" ) + current_seconds;
			var timeOfDay = ( real_hours < 12 ) ? "AM" : "PM";
			current_hours = ( current_hours > 12 ) ? current_hours - 12 : current_hours;
			current_hours = ( current_hours == 0 ) ? 12 : current_hours;
			var currentTimeString = current_hours + ":" + current_minutes + ":" + current_seconds + " " + timeOfDay;
			document.getElementById("clock").firstChild.nodeValue = currentTimeString;
			current_seconds++;
			current_minutes=parseInt(current_minutes);
  
			if(current_seconds>=60){
			    current_seconds=0;
			    current_minutes++;
			}
  
			if(current_minutes>=60){
				current_minutes=0;
				current_seconds=0;
				current_hours++;
				real_hours++;
			}

			if(real_hours==13){
			    current_hours=1;
			}

			if(real_hours>=24){
				real_hours=0;
				current_hours=0;
				current_minutes=0;
				current_seconds=0;
			}
		};
	});
})(jQuery);
